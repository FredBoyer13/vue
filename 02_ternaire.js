
/**
 * retourne la valeur la plus grande des deux paramètres
 * 
 * Contraintes:
 *    - utiliser l'opérateur ternaire (if interdit)
 */
function ternaire(a , b) {
	let ret = a > b ? a : b ;
	
	return ret;
}

module.exports = ternaire;