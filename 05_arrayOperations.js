
/**
 * Utiliser la fonction .map sur le tableau passé en paramètre
 * pour retourner un nouveau tableau avec les valeurs multipliées par 2
 * 
 * contraintes: 
 *   - Les mots clées for, while, do while sont interdits
 *   - les mots clées function et return sont interdits
 *   - Vous ne pouvez pas utiliser de variable
 * 
  */

const multiplyByTwo = (array) => array.map((x) => x * 2);
let tab = [1, 2, 3, 4, 5, 6, 7, 8];


/**
 * Utiliser la fonction .filter sur le tableau passé en paramètre
 * retourne un nouveau tableau avec uniquement les nom qui commencent par la lettre "A"
 * 
 * contraintes: 
 *   - Les mots clées for, while, do while sont interdits
 *   - les mots clées function et return sont interdits
 *   - Vous ne pouvez pas utiliser de variable (autre que l'argument de la fonction)
  */
//let tab2 = ["ananas", "banane", "abricot"];
//const filterNameStartByA= (words) => words.filter((word) => filter((array) => word.substr(0, 1) == 'a')

let tab2 = ["ananas", "banane", "abricot"];


const filterNameStartByA = tabmot => tabmot.filter((word) => word.substr(0, 1) == 'a');


/**
 * Utiliser la fonction .reduce sur le tableau passé en paramètre
 * retourne la somme des valeurs du tableau
 * 
 * contraintes: 
 *   - Les mots clées for, while, do while sont interdits
 *   - les mots clées function et return sont interdits
 *   - Vous ne pouvez pas utiliser de variable (autre que l'argument de la fonction)
  */

const array1 = [1, 2, 3, 4];

const sumWithInitial = array => array.reduce((accumulator, currentValue) => accumulator + currentValue, 0);


const sum = (array) => {}

/**
 * Utiliser la fonction .find sur le tableau passé en paramètre
 * retourne l'utilisateur qui a l'id passé en 2e paramètre
 * 
 * exemple d'entrée:
 * [
 *  {id: 1, name: 'John'},
 *  {id: 2, name: 'Doe'},
 *  {id: 3, name: 'Foo'},
 *  {id: 4, name: 'Bar'},
 * ], 3
 * retour attendu: "Foo"
 * 
 * contraintes: 
 *   - Les mots clées for, while, do while sont interdits
 *   - les mots clées function et return sont interdits
 *   - Vous ne pouvez pas utiliser de variable (autre que l'argument de la fonction)
  */


const liste = [ {id: 1, name: 'John'},
   {id: 2, name: 'Doe'},
   {id: 3, name: 'Foo'},
   {id: 4, name: 'Bar'},];
console.log(liste[2]);
//const found = liste.find((element) => element > 10);

//console.log(found);

const findUserById = (array, id) => {}


module.exports = {multiplyByTwo, filterNameStartByA, sum, findUserById};